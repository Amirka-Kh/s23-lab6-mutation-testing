from bonus_system import calculateBonuses
from random import choice

program_mappings = {"Standard": 0.5, "Premium": 0.1, "Diamond": 0.2}
amounts_mappings = {
    9999: 1, 10000: 1.5, 10001: 1.5,
    49999: 1.5, 50000: 2, 50001: 2,
    99999: 2, 100000: 2.5, 100001: 2.5
}

def calculate_bonus(amount):
    bonus = 1 if amount < 10000 else \
            1.5 if amount < 50000 else \
            2 if amount < 100000 else 2.5
    return bonus
    
def test_invalid_program():
    for program in 'qwertyuiopasdfghjklzxcvbnm':
        for amount in amounts_mappings.keys():
            assert calculateBonuses(program, amount) == 0

def test_valid_amount():
    for program in program_mappings.keys():
        for amount in amounts_mappings.keys():
            assert calculateBonuses(program, amount) == amounts_mappings[amount]*program_mappings[program]
